﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BP2CL.Model;

namespace BP2CL.ViewModel
{
    public class UpravljanjaVM
    {
        public static BindingList<Vozi> upravljanjaList;

        public static int Create(int jmbg, int br, int id)
        {
            Vozi upravljanje = new Vozi()
            {
                UpravljaVozacJMBG = jmbg,
                VoziloBROJ_SASIJE = br,
                UpravljaTipVozilaID_KATEGORIJE = id
            };

            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                database.VoziSet.Add(upravljanje);

                database.SaveChanges();

                upravljanjaList.Add(upravljanje);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static void Read()
        {
            DataBaseContainer1 database = new DataBaseContainer1();
            upravljanjaList = new BindingList<Vozi>();

            foreach (Vozi jedno_upravljanje in database.VoziSet)
            {
                Vozi upravljanje = new Vozi()
                {
                    UpravljaVozacJMBG = jedno_upravljanje.UpravljaVozacJMBG,
                    UpravljaTipVozilaID_KATEGORIJE = jedno_upravljanje.UpravljaTipVozilaID_KATEGORIJE,
                    VoziloBROJ_SASIJE = jedno_upravljanje.VoziloBROJ_SASIJE
                };

                upravljanjaList.Add(upravljanje);
            }
        }

        public static int Update(int index, string ime, DateTime datum, int vazenje)
        {
            return 1;
        }

        public static int Delete(int index)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                if (!database.VoziSet.Local.Contains(upravljanjaList[index]))
                {
                    database.VoziSet.Attach(upravljanjaList[index]);
                }

                database.VoziSet.Remove(upravljanjaList[index]);

                database.SaveChanges();

                upravljanjaList.RemoveAt(index);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }
    }
}

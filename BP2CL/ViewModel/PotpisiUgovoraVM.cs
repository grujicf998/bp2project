﻿using BP2CL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BP2CL.ViewModel
{
    public class PotpisiUgovoraVM
    {
        public static BindingList<Potpisuje> potpisiUgovoraList;

        public static int Create(int jmbg_potpisnika, int id_ugovora)
        {
            Potpisuje potpis = new Potpisuje()
            {
                DirektorJMBG = jmbg_potpisnika,
                UgovorID_UGOVORA = id_ugovora
            };

            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                database.PotpisujeSet.Add(potpis);

                database.SaveChanges();

                potpisiUgovoraList.Add(potpis);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static void Read()
        {
            DataBaseContainer1 database = new DataBaseContainer1();
            potpisiUgovoraList = new BindingList<Potpisuje>();

            foreach (Potpisuje jedan_potpis in database.PotpisujeSet)
            {
                Potpisuje potpis = new Potpisuje()
                {
                    DirektorJMBG = jedan_potpis.DirektorJMBG,
                    UgovorID_UGOVORA = jedan_potpis.UgovorID_UGOVORA
                };

                potpisiUgovoraList.Add(potpis);
            }
        }

        public static int Update(int index, string ime, DateTime datum, int vazenje)
        {
            return 1;
        }

        public static int Delete(int index)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            int id = potpisiUgovoraList[index].UgovorID_UGOVORA;
            int counter = 0;

            foreach (Potpisuje jedan_potpis in potpisiUgovoraList)
            {
                if (jedan_potpis.UgovorID_UGOVORA == id)
                    counter++;
            }

            if (counter < 2)
                return -1;

            try
            {
                if (!database.PotpisujeSet.Local.Contains(potpisiUgovoraList[index]))
                {
                    database.PotpisujeSet.Attach(potpisiUgovoraList[index]);
                }

                database.PotpisujeSet.Remove(potpisiUgovoraList[index]);

                database.SaveChanges();

                potpisiUgovoraList.RemoveAt(index);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }
    }
}

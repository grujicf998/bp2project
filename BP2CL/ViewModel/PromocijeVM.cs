﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BP2CL.Model;

namespace BP2CL.ViewModel
{
    public class PromocijeVM
    {
        public static BindingList<Promocija> promocijeList;

        public static int Create(int id_promocije, int cena, string vrsta, int jmbg)
        {
            Promocija promocija = new Promocija()
            {
                ID_PROMOCIJE = id_promocije,
                CENA_PROMOCIJE = cena,
                VRSTA_PROMOCIJE = vrsta
            };

            DataBaseContainer1 database = new DataBaseContainer1();

            Kreira kreacija = new Kreira()
            {
                PromocijaID_PROMOCIJE = id_promocije,
                PromoterJMBG = jmbg
            };

            try
            {
                database.PromocijaSet.Add(promocija);

                database.KreiraSet1.Add(kreacija);

                database.SaveChanges();

                promocijeList.Add(promocija);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static void Read()
        {
            DataBaseContainer1 database = new DataBaseContainer1();
            promocijeList = new BindingList<Promocija>();

            foreach (Promocija jedna_promocija in database.PromocijaSet)
            {
                Promocija promocija = new Promocija()
                {
                    ID_PROMOCIJE = jedna_promocija.ID_PROMOCIJE,
                    CENA_PROMOCIJE = jedna_promocija.CENA_PROMOCIJE,
                    VRSTA_PROMOCIJE = jedna_promocija.VRSTA_PROMOCIJE
                };

                promocijeList.Add(promocija);
            }
        }

        public static int Update(int index, int cena, string vrsta)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            int id = promocijeList[index].ID_PROMOCIJE;

            try
            {
                var promocija = database.PromocijaSet.FirstOrDefault(x => x.ID_PROMOCIJE == id);
                promocija.ID_PROMOCIJE = id;
                promocija.CENA_PROMOCIJE = cena;
                promocija.VRSTA_PROMOCIJE = vrsta;

                database.SaveChanges();

                promocijeList[index].ID_PROMOCIJE = id;
                promocijeList[index].CENA_PROMOCIJE = cena;
                promocijeList[index].VRSTA_PROMOCIJE = vrsta;
                promocijeList.ResetBindings();
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static int Delete(int index)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                if (!database.PromocijaSet.Local.Contains(promocijeList[index]))
                {
                    database.PromocijaSet.Attach(promocijeList[index]);
                }

                database.PromocijaSet.Remove(promocijeList[index]);

                database.SaveChanges();

                promocijeList.RemoveAt(index);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static BindingList<int> getPromocije()
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            BindingList<int> retList = new BindingList<int>();

            foreach (Promocija jedna_promocija in database.PromocijaSet)
            {
                retList.Add(jedna_promocija.ID_PROMOCIJE);
            }

            return retList;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BP2CL.Model;

namespace BP2CL.ViewModel
{
    public class PorudzbineVM
    {
        public static BindingList<Porudzbina> porudzbineList;

        public static int Create(int rb, int cena, string porucilac, string predmet, int jmbg)
        {
            Porudzbina porudzbina = new Porudzbina()
            {
                RB_PORUDZBINE = rb,
                CENA_PORUDZBINE = cena,
                IME_PORUCIOCA = porucilac,
                PREDMET_PORUDZBINE = predmet,
                DispecerJMBG = jmbg
            };

            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                database.PorudzbinaSet.Add(porudzbina);

                database.SaveChanges();

                porudzbineList.Add(porudzbina);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static void Read()
        {
            DataBaseContainer1 database = new DataBaseContainer1();
            porudzbineList = new BindingList<Porudzbina>();

            foreach (Porudzbina jedna_porudzbina in database.PorudzbinaSet)
            {
                Porudzbina porudzbina = new Porudzbina()
                {
                    RB_PORUDZBINE = jedna_porudzbina.RB_PORUDZBINE,
                    CENA_PORUDZBINE = jedna_porudzbina.CENA_PORUDZBINE,
                    PREDMET_PORUDZBINE = jedna_porudzbina.PREDMET_PORUDZBINE,
                    IME_PORUCIOCA = jedna_porudzbina.IME_PORUCIOCA,
                    DispecerJMBG = jedna_porudzbina.DispecerJMBG
                };

                porudzbineList.Add(porudzbina);
            }
        }

        public static int Update(int index, int cena, string porucilac, string predmet, int jmbg)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            int rb = porudzbineList[index].RB_PORUDZBINE;

            try
            {
                var porudzbina = database.PorudzbinaSet.FirstOrDefault(x => x.RB_PORUDZBINE == rb);
                porudzbina.RB_PORUDZBINE = rb;
                porudzbina.CENA_PORUDZBINE = cena;
                porudzbina.IME_PORUCIOCA = porucilac;
                porudzbina.PREDMET_PORUDZBINE = predmet;
                porudzbina.DispecerJMBG = jmbg;

                database.SaveChanges();

                porudzbineList[index].RB_PORUDZBINE = rb;
                porudzbineList[index].CENA_PORUDZBINE = cena;
                porudzbineList[index].IME_PORUCIOCA = porucilac;
                porudzbineList[index].PREDMET_PORUDZBINE = predmet;
                porudzbineList[index].DispecerJMBG = jmbg;
                porudzbineList.ResetBindings();
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static int Delete(int index)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                if (!database.PorudzbinaSet.Local.Contains(porudzbineList[index]))
                {
                    database.PorudzbinaSet.Attach(porudzbineList[index]);
                }

                database.PorudzbinaSet.Remove(porudzbineList[index]);

                database.SaveChanges();

                porudzbineList.RemoveAt(index);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }
    }
}

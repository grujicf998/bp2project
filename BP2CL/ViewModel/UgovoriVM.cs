﻿using BP2CL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BP2CL.ViewModel
{
    public class UgovoriVM
    {
        public static BindingList<Ugovor> ugovoriList;

        public static int Create(int id_ugovora, string ime_kompanije, int period_vaznje, DateTime datum_zakljucenja, int jmbg)
        {
            Ugovor ugovor = new Ugovor()
            {
                ID_UGOVORA = id_ugovora,
                IME_KOMPANIJE = ime_kompanije,
                PERIOD_VAZENJA = period_vaznje,
                DAT_ZAKLJUCENJA = datum_zakljucenja
            };

            DataBaseContainer1 database = new DataBaseContainer1();

            Potpisuje potpisuje = new Potpisuje()
            {
                UgovorID_UGOVORA = id_ugovora,
                DirektorJMBG = jmbg
            };

            try
            {
                database.UgovorSet.Add(ugovor);

                database.PotpisujeSet.Add(potpisuje);

                database.SaveChanges();

                ugovoriList.Add(ugovor);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static void Read()
        {
            DataBaseContainer1 database = new DataBaseContainer1();
            ugovoriList = new BindingList<Ugovor>();

            foreach (Ugovor jedan_ugovor in database.UgovorSet)
            {
                Ugovor ugovor = new Ugovor()
                {
                    ID_UGOVORA = jedan_ugovor.ID_UGOVORA,
                    IME_KOMPANIJE = jedan_ugovor.IME_KOMPANIJE,
                    DAT_ZAKLJUCENJA = jedan_ugovor.DAT_ZAKLJUCENJA,
                    PERIOD_VAZENJA = jedan_ugovor.PERIOD_VAZENJA
                };

                ugovoriList.Add(ugovor);
            }
        }

        public static int Update(int index, string ime, DateTime datum, int vazenje)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            int id = ugovoriList[index].ID_UGOVORA;

            try
            {
                var ugovor = database.UgovorSet.FirstOrDefault(x => x.ID_UGOVORA == id);
                ugovor.IME_KOMPANIJE = ime;
                ugovor.DAT_ZAKLJUCENJA = datum;
                ugovor.PERIOD_VAZENJA = vazenje;

                database.SaveChanges();

                ugovoriList[index].IME_KOMPANIJE = ime;
                ugovoriList[index].DAT_ZAKLJUCENJA = datum;
                ugovoriList[index].PERIOD_VAZENJA = vazenje;
                ugovoriList.ResetBindings();
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static int Delete(int index)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                if (!database.UgovorSet.Local.Contains(ugovoriList[index]))
                {
                    database.UgovorSet.Attach(ugovoriList[index]);
                }

                database.UgovorSet.Remove(ugovoriList[index]);

                database.SaveChanges();

                ugovoriList.RemoveAt(index);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static BindingList<int> getUgovori()
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            BindingList<int> retList = new BindingList<int>();

            foreach (Ugovor jedan_ugovor in database.UgovorSet)
            {
                    retList.Add(jedan_ugovor.ID_UGOVORA);
            }

            return retList;
        }
    }
}

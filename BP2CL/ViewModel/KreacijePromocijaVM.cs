﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BP2CL.Model;

namespace BP2CL.ViewModel
{
    public class KreacijePromocijaVM
    {
        public static BindingList<Kreira> kracijePromocijaList;

        public static int Create(int jmbg_kreatora, int id_promocije)
        {
            Kreira kreacija = new Kreira()
            {
                PromoterJMBG = jmbg_kreatora,
                PromocijaID_PROMOCIJE = id_promocije
            };

            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                database.KreiraSet1.Add(kreacija);

                database.SaveChanges();

                kracijePromocijaList.Add(kreacija);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static void Read()
        {
            DataBaseContainer1 database = new DataBaseContainer1();
            kracijePromocijaList = new BindingList<Kreira>();

            foreach (Kreira jedna_kreacija in database.KreiraSet1)
            {
                Kreira kreacija = new Kreira()
                {
                    PromoterJMBG = jedna_kreacija.PromoterJMBG,
                    PromocijaID_PROMOCIJE = jedna_kreacija.PromocijaID_PROMOCIJE
                };

                kracijePromocijaList.Add(kreacija);
            }
        }

        public static int Update(int index, string ime, DateTime datum, int vazenje)
        {
            return 1;
        }

        public static int Delete(int index)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            int id = kracijePromocijaList[index].PromocijaID_PROMOCIJE;
            int counter = 0;

            foreach (Kreira jedna_kreacija in kracijePromocijaList)
            {
                if (jedna_kreacija.PromocijaID_PROMOCIJE == id)
                    counter++;
            }

            if (counter < 2)
                return -1;

            try
            {
                if (!database.KreiraSet1.Local.Contains(kracijePromocijaList[index]))
                {
                    database.KreiraSet1.Attach(kracijePromocijaList[index]);
                }

                database.KreiraSet1.Remove(kracijePromocijaList[index]);

                database.SaveChanges();

                kracijePromocijaList.RemoveAt(index);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }
    }
}

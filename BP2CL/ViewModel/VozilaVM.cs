﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BP2CL.Model;

namespace BP2CL.ViewModel
{
    public class VozilaVM
    {
        public static BindingList<Vozilo> vozilaList;

        public static int Create(int br_sasije, string marka, string model, string tablice, int kategorija)
        {
            Vozilo vozilo = new Vozilo()
            {
                BROJ_SASIJE = br_sasije,
                MARKA = marka,
                MODEL = model,
                RG_TABLICE = tablice,
                TipVozilaID_KATEGORIJE = kategorija
            };

            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                database.VoziloSet.Add(vozilo);

                database.SaveChanges();

                vozilaList.Add(vozilo);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static void Read()
        {
            DataBaseContainer1 database = new DataBaseContainer1();
            vozilaList = new BindingList<Vozilo>();

            foreach (Vozilo jedno_vozilo in database.VoziloSet)
            {
                Vozilo vozilo = new Vozilo()
                {
                    BROJ_SASIJE = jedno_vozilo.BROJ_SASIJE,
                    RG_TABLICE = jedno_vozilo.RG_TABLICE,
                    MARKA = jedno_vozilo.MARKA,
                    MODEL = jedno_vozilo.MODEL,
                    TipVozilaID_KATEGORIJE = jedno_vozilo.TipVozilaID_KATEGORIJE
                };

                vozilaList.Add(vozilo);
            }
        }

        public static int Update(int index, string marka, string model, string tablice, int kategorija)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            int br_sasije = vozilaList[index].BROJ_SASIJE;

            try
            {
                var vozilo = database.VoziloSet.FirstOrDefault(x => x.BROJ_SASIJE == br_sasije);
                vozilo.MARKA = marka;
                vozilo.MODEL = model;
                vozilo.RG_TABLICE = tablice;
                vozilo.TipVozilaID_KATEGORIJE = kategorija;

                database.SaveChanges();

                vozilaList[index].MARKA = marka;
                vozilaList[index].MODEL = model;
                vozilaList[index].RG_TABLICE = tablice;
                vozilaList[index].TipVozilaID_KATEGORIJE = kategorija;
                vozilaList.ResetBindings();
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static int Delete(int index)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                if (!database.VoziloSet.Local.Contains(vozilaList[index]))
                {
                    database.VoziloSet.Attach(vozilaList[index]);
                }

                database.VoziloSet.Remove(vozilaList[index]);

                database.SaveChanges();

                vozilaList.RemoveAt(index);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static BindingList<string> getVozila()
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            BindingList<string> retList = new BindingList<string>();

            foreach (Vozilo jedno_vozilo in database.VoziloSet)
            {
                retList.Add(jedno_vozilo.BROJ_SASIJE + "-" + jedno_vozilo.TipVozilaID_KATEGORIJE);
            }

            return retList;
        }
    }
}

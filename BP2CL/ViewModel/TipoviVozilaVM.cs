﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BP2CL.Model;

namespace BP2CL.ViewModel
{
    public class TipoviVozilaVM
    {
        public static BindingList<TipVozila> tipoviList;

        public static int Create(int id, string ime, int godine)
        {
            TipVozila tip = new TipVozila()
            {
                ID_KATEGORIJE = id,
                KATEGORIJA = ime,
                POTREBNE_GOD = godine,
            };

            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                database.TipVozilaSet.Add(tip);

                database.TipVozilaSet.Add(tip);

                database.SaveChanges();

                tipoviList.Add(tip);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static void Read()
        {
            DataBaseContainer1 database = new DataBaseContainer1();
            tipoviList = new BindingList<TipVozila>();

            foreach (TipVozila jedan_tip in database.TipVozilaSet)
            {
                TipVozila tip = new TipVozila()
                {
                    ID_KATEGORIJE = jedan_tip.ID_KATEGORIJE,
                    KATEGORIJA = jedan_tip.KATEGORIJA,
                    POTREBNE_GOD = jedan_tip.POTREBNE_GOD,
                };

                tipoviList.Add(tip);
            }
        }

        public static int Update(int index, string ime, int godine)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            int id = tipoviList[index].ID_KATEGORIJE;

            try
            {
                var tip = database.TipVozilaSet.FirstOrDefault(x => x.ID_KATEGORIJE == id);
                tip.ID_KATEGORIJE = id;
                tip.KATEGORIJA = ime;
                tip.POTREBNE_GOD = godine;

                database.SaveChanges();

                tipoviList[index].ID_KATEGORIJE = id;
                tipoviList[index].KATEGORIJA = ime;
                tipoviList[index].POTREBNE_GOD = godine;
                tipoviList.ResetBindings();
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static int Delete(int index)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                if (!database.TipVozilaSet.Local.Contains(tipoviList[index]))
                {
                    database.TipVozilaSet.Attach(tipoviList[index]);
                }

                database.TipVozilaSet.Remove(tipoviList[index]);

                database.SaveChanges();

                tipoviList.RemoveAt(index);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static BindingList<int> getKategorije()
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            BindingList<int> retList = new BindingList<int>();

            foreach (TipVozila jedan_tip in database.TipVozilaSet)
            {
                retList.Add(jedan_tip.ID_KATEGORIJE);
            }

            return retList;
        }
    }
}

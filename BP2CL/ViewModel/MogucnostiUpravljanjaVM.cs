﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BP2CL.Model;

namespace BP2CL.ViewModel
{
    public class MogucnostiUpravljanjaVM
    {
        public static BindingList<Upravlja> mogucnostiUpravljanjaList;

        public static int Create(int jmbg_vozaca, int id_kategorije)
        {
            Upravlja upravljanje = new Upravlja()
            {
                VozacJMBG = jmbg_vozaca,
                TipVozilaID_KATEGORIJE = id_kategorije
            };

            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                database.UpravljaSet.Add(upravljanje);

                database.SaveChanges();

                mogucnostiUpravljanjaList.Add(upravljanje);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static void Read()
        {
            DataBaseContainer1 database = new DataBaseContainer1();
            mogucnostiUpravljanjaList = new BindingList<Upravlja>();

            foreach (Upravlja jedno_upravljanje in database.UpravljaSet)
            {
                Upravlja upravljanje = new Upravlja()
                {
                    VozacJMBG = jedno_upravljanje.VozacJMBG,
                    TipVozilaID_KATEGORIJE = jedno_upravljanje.TipVozilaID_KATEGORIJE
                };

                mogucnostiUpravljanjaList.Add(upravljanje);
            }
        }

        public static int Update(int index, string ime, DateTime datum, int vazenje)
        {
            return 1;
        }

        public static int Delete(int index)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            int jmbg = mogucnostiUpravljanjaList[index].VozacJMBG;
            int counter = 0;

            foreach (Upravlja jedno_upravljanje in mogucnostiUpravljanjaList)
            {
                if (jedno_upravljanje.VozacJMBG == jmbg)
                    counter++;
            }

            if (counter < 2)
                return -1;

            try
            {
                if (!database.UpravljaSet.Local.Contains(mogucnostiUpravljanjaList[index]))
                {
                    database.UpravljaSet.Attach(mogucnostiUpravljanjaList[index]);
                }

                database.UpravljaSet.Remove(mogucnostiUpravljanjaList[index]);

                database.SaveChanges();

                mogucnostiUpravljanjaList.RemoveAt(index);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static BindingList<string> getMogucnostiUpravljanja()
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            BindingList<string> retList = new BindingList<string>();

            foreach (Upravlja jedna_mogucnost in database.UpravljaSet)
            {
                retList.Add(jedna_mogucnost.VozacJMBG + "-" + jedna_mogucnost.TipVozilaID_KATEGORIJE);
            }

            return retList;
        }
    }
}

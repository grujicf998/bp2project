﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BP2CL.Model;

namespace BP2CL.ViewModel
{
    public class RadniciVM
    {
        public static BindingList<Radnik> radniciList;

        public static int Create(int jmbg, string ime, string prezime, int plata, string zanimanje, int kategorija)
        {
            if (zanimanje == "Direktor")
            {
                Direktor radnik = new Direktor()
                {
                    JMBG = jmbg,
                    IME = ime,
                    PREZIME = prezime,
                    PLATA = plata,
                    ZANIMANJE = zanimanje
                };

                DataBaseContainer1 database = new DataBaseContainer1();

                try
                {
                    database.RadnikSet.Add(radnik);

                    database.SaveChanges();

                    radniciList.Add(radnik);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
            else if (zanimanje == "Promoter")
            {
                Promoter radnik = new Promoter()
                {
                    JMBG = jmbg,
                    IME = ime,
                    PREZIME = prezime,
                    PLATA = plata,
                    ZANIMANJE = zanimanje
                };

                DataBaseContainer1 database = new DataBaseContainer1();

                try
                {
                    database.RadnikSet.Add(radnik);

                    database.SaveChanges();

                    radniciList.Add(radnik);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
            else if (zanimanje == "Dispecer")
            {
                Dispecer radnik = new Dispecer()
                {
                    JMBG = jmbg,
                    IME = ime,
                    PREZIME = prezime,
                    PLATA = plata,
                    ZANIMANJE = zanimanje
                };

                DataBaseContainer1 database = new DataBaseContainer1();

                try
                {
                    database.RadnikSet.Add(radnik);

                    database.SaveChanges();

                    radniciList.Add(radnik);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
            else if (zanimanje == "Vozac")
            {
                Vozac radnik = new Vozac()
                {
                    JMBG = jmbg,
                    IME = ime,
                    PREZIME = prezime,
                    PLATA = plata,
                    ZANIMANJE = zanimanje
                };

                Upravlja upravljanje = new Upravlja()
                {
                    VozacJMBG = jmbg,
                    TipVozilaID_KATEGORIJE = kategorija
                };

                DataBaseContainer1 database = new DataBaseContainer1();

                try
                {
                    database.RadnikSet.Add(radnik);

                    database.SaveChanges();

                    database.UpravljaSet.Add(upravljanje);

                    database.SaveChanges();

                    radniciList.Add(radnik);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }

            return 1;
        }

        public static void Read()
        {
            DataBaseContainer1 database = new DataBaseContainer1();
            radniciList = new BindingList<Radnik>();

            foreach (Radnik jedan_radnik in database.RadnikSet)
            {
                if (jedan_radnik.ZANIMANJE == "Direktor")
                {
                    Direktor radnik = new Direktor()
                    {
                        JMBG = jedan_radnik.JMBG,
                        IME = jedan_radnik.IME,
                        PREZIME = jedan_radnik.PREZIME,
                        PLATA = jedan_radnik.PLATA,
                        ZANIMANJE = jedan_radnik.ZANIMANJE
                    };
                    radniciList.Add(radnik);
                }
                else if (jedan_radnik.ZANIMANJE == "Vozac")
                {
                    Vozac radnik = new Vozac()
                    {
                        JMBG = jedan_radnik.JMBG,
                        IME = jedan_radnik.IME,
                        PREZIME = jedan_radnik.PREZIME,
                        PLATA = jedan_radnik.PLATA,
                        ZANIMANJE = jedan_radnik.ZANIMANJE
                    };
                    radniciList.Add(radnik);
                }
                else if (jedan_radnik.ZANIMANJE == "Promoter")
                {
                    Promoter radnik = new Promoter()
                    {
                        JMBG = jedan_radnik.JMBG,
                        IME = jedan_radnik.IME,
                        PREZIME = jedan_radnik.PREZIME,
                        PLATA = jedan_radnik.PLATA,
                        ZANIMANJE = jedan_radnik.ZANIMANJE
                    };
                    radniciList.Add(radnik);
                }
                else if (jedan_radnik.ZANIMANJE == "Dispecer")
                {
                    Dispecer radnik = new Dispecer()
                    {
                        JMBG = jedan_radnik.JMBG,
                        IME = jedan_radnik.IME,
                        PREZIME = jedan_radnik.PREZIME,
                        PLATA = jedan_radnik.PLATA,
                        ZANIMANJE = jedan_radnik.ZANIMANJE
                    };
                    radniciList.Add(radnik);
                }
            }
        }

        public static int Update(int index, string ime, string prezime, int plata)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            int jmbg = radniciList[index].JMBG;

            try
            {
                var radnik = database.RadnikSet.FirstOrDefault(x => x.JMBG == jmbg);
                radnik.IME = ime;
                radnik.PREZIME = prezime;
                radnik.PLATA = plata;

                database.SaveChanges();

                radniciList[index].IME = ime;
                radniciList[index].PREZIME = prezime;
                radniciList[index].PLATA = plata;
                radniciList.ResetBindings();
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static int Delete(int index)
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            try
            {
                if (!database.RadnikSet.Local.Contains(radniciList[index]))
                {
                    database.RadnikSet.Attach(radniciList[index]);
                }

                database.RadnikSet.Remove(radniciList[index]);
                
                database.SaveChanges();

                radniciList.RemoveAt(index);
            }
            catch (Exception)
            {
                return -1;
            }

            return 1;
        }

        public static BindingList<int> getDirektori()
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            BindingList<int> retList = new BindingList<int>();

            foreach (Radnik jedan_direktor in database.RadnikSet)
            {
                if (jedan_direktor.ZANIMANJE == "Direktor")
                    retList.Add(jedan_direktor.JMBG);
            }

            return retList;
        }

        public static BindingList<int> getPromoteri()
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            BindingList<int> retList = new BindingList<int>();

            foreach (Radnik jedan_promoter in database.RadnikSet)
            {
                if (jedan_promoter.ZANIMANJE == "Promoter")
                    retList.Add(jedan_promoter.JMBG);
            }

            return retList;
        }

        public static BindingList<int> getDispeceri()
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            BindingList<int> retList = new BindingList<int>();

            foreach (Radnik jedan_dispecer in database.RadnikSet)
            {
                if (jedan_dispecer.ZANIMANJE == "Dispecer")
                    retList.Add(jedan_dispecer.JMBG);
            }

            return retList;
        }

        public static BindingList<int> getVozaci()
        {
            DataBaseContainer1 database = new DataBaseContainer1();

            BindingList<int> retList = new BindingList<int>();

            foreach (Radnik jedan_dispecer in database.RadnikSet)
            {
                if (jedan_dispecer.ZANIMANJE == "Vozac")
                    retList.Add(jedan_dispecer.JMBG);
            }

            return retList;
        }
    }
}

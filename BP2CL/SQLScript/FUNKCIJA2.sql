CREATE FUNCTION FUNKCIJA2(@VRSTA_PROMOCIJE VARCHAR(255))
RETURNS DECIMAL
AS
BEGIN
DECLARE
@return_value AS DECIMAL;
select @return_value = count(distinct JMBG) from RadnikSet r, KreiraSet1 k, PromocijaSet p
	where r.JMBG = k.PromoterJMBG and k.PromocijaID_PROMOCIJE = p.ID_PROMOCIJE and p.VRSTA_PROMOCIJE = @VRSTA_PROMOCIJE;
RETURN @return_value;
END; 

--declare @BROJ_DIREKTORA INT;
--execute @BROJ_DIREKTORA = FUNKCIJA2 'OGLAS';
--select @BROJ_DIREKTORA;

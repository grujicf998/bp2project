CREATE FUNCTION FUNKCIJA1()
RETURNS DECIMAL
AS
BEGIN
DECLARE
@return_value AS DECIMAL;
select @return_value = count(distinct JMBG) from RadnikSet r, PotpisujeSet p, UgovorSet u
	where r.JMBG = p.DirektorJMBG and p.UgovorID_UGOVORA = u.ID_UGOVORA and u.PERIOD_VAZENJA >= 4;
RETURN @return_value;
END; 

--declare @BROJ_DIREKTORA INT;
--execute @BROJ_DIREKTORA = FUNKCIJA1;
--select @BROJ_DIREKTORA;

-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/30/2021 20:12:16
-- Generated from EDMX file: D:\BP2Project\BP2CL\DataBase.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [DataBaseBP2];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_DirektorPotpisuje]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PotpisujeSet] DROP CONSTRAINT [FK_DirektorPotpisuje];
GO
IF OBJECT_ID(N'[dbo].[FK_UgovorPotpisuje]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PotpisujeSet] DROP CONSTRAINT [FK_UgovorPotpisuje];
GO
IF OBJECT_ID(N'[dbo].[FK_PromoterKreira]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[KreiraSet] DROP CONSTRAINT [FK_PromoterKreira];
GO
IF OBJECT_ID(N'[dbo].[FK_PromocijaKreira]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[KreiraSet] DROP CONSTRAINT [FK_PromocijaKreira];
GO
IF OBJECT_ID(N'[dbo].[FK_DispecerPorudzbina]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PorudzbinaSet] DROP CONSTRAINT [FK_DispecerPorudzbina];
GO
IF OBJECT_ID(N'[dbo].[FK_VozacUpravlja]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UpravljaSet] DROP CONSTRAINT [FK_VozacUpravlja];
GO
IF OBJECT_ID(N'[dbo].[FK_TipVozilaUpravlja]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UpravljaSet] DROP CONSTRAINT [FK_TipVozilaUpravlja];
GO
IF OBJECT_ID(N'[dbo].[FK_TipVozilaVozilo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VoziloSet] DROP CONSTRAINT [FK_TipVozilaVozilo];
GO
IF OBJECT_ID(N'[dbo].[FK_UpravljaVozi]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VoziSet] DROP CONSTRAINT [FK_UpravljaVozi];
GO
IF OBJECT_ID(N'[dbo].[FK_VoziloVozi]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VoziSet] DROP CONSTRAINT [FK_VoziloVozi];
GO
IF OBJECT_ID(N'[dbo].[FK_Direktor_inherits_Radnik]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RadnikSet_Direktor] DROP CONSTRAINT [FK_Direktor_inherits_Radnik];
GO
IF OBJECT_ID(N'[dbo].[FK_Promoter_inherits_Radnik]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RadnikSet_Promoter] DROP CONSTRAINT [FK_Promoter_inherits_Radnik];
GO
IF OBJECT_ID(N'[dbo].[FK_Dispecer_inherits_Radnik]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RadnikSet_Dispecer] DROP CONSTRAINT [FK_Dispecer_inherits_Radnik];
GO
IF OBJECT_ID(N'[dbo].[FK_Vozac_inherits_Radnik]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RadnikSet_Vozac] DROP CONSTRAINT [FK_Vozac_inherits_Radnik];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[RadnikSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RadnikSet];
GO
IF OBJECT_ID(N'[dbo].[UgovorSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UgovorSet];
GO
IF OBJECT_ID(N'[dbo].[PotpisujeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PotpisujeSet];
GO
IF OBJECT_ID(N'[dbo].[PromocijaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PromocijaSet];
GO
IF OBJECT_ID(N'[dbo].[KreiraSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[KreiraSet];
GO
IF OBJECT_ID(N'[dbo].[PorudzbinaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PorudzbinaSet];
GO
IF OBJECT_ID(N'[dbo].[VoziloSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VoziloSet];
GO
IF OBJECT_ID(N'[dbo].[TipVozilaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TipVozilaSet];
GO
IF OBJECT_ID(N'[dbo].[UpravljaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UpravljaSet];
GO
IF OBJECT_ID(N'[dbo].[VoziSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VoziSet];
GO
IF OBJECT_ID(N'[dbo].[RadnikSet_Direktor]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RadnikSet_Direktor];
GO
IF OBJECT_ID(N'[dbo].[RadnikSet_Promoter]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RadnikSet_Promoter];
GO
IF OBJECT_ID(N'[dbo].[RadnikSet_Dispecer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RadnikSet_Dispecer];
GO
IF OBJECT_ID(N'[dbo].[RadnikSet_Vozac]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RadnikSet_Vozac];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'RadnikSet'
CREATE TABLE [dbo].[RadnikSet] (
    [JMBG] int  NOT NULL,
    [IME] nvarchar(max)  NOT NULL,
    [PREZIME] nvarchar(max)  NOT NULL,
    [PLATA] int  NOT NULL,
    [ZANIMANJE] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'UgovorSet'
CREATE TABLE [dbo].[UgovorSet] (
    [ID_UGOVORA] int IDENTITY(1,1) NOT NULL,
    [IME_KOMPANIJE] nvarchar(max)  NOT NULL,
    [PERIOD_VAZENJA] nvarchar(max)  NOT NULL,
    [DAT_ZAKLJUCENJA] datetime  NOT NULL
);
GO

-- Creating table 'PotpisujeSet'
CREATE TABLE [dbo].[PotpisujeSet] (
    [DirektorJMBG] int  NOT NULL,
    [UgovorID_UGOVORA] int  NOT NULL
);
GO

-- Creating table 'PromocijaSet'
CREATE TABLE [dbo].[PromocijaSet] (
    [ID_PROMOCIJE] int IDENTITY(1,1) NOT NULL,
    [CENA_PROMOCIJE] int  NOT NULL,
    [VRSTA_PROMOCIJE] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'KreiraSet'
CREATE TABLE [dbo].[KreiraSet] (
    [PromoterJMBG] int  NOT NULL,
    [PromocijaID_PROMOCIJE] int  NOT NULL
);
GO

-- Creating table 'PorudzbinaSet'
CREATE TABLE [dbo].[PorudzbinaSet] (
    [RB_PORUDZBINE] int IDENTITY(1,1) NOT NULL,
    [CENA_PORUDZBINE] int  NOT NULL,
    [PREDMET_PORUDZBINE] nvarchar(max)  NOT NULL,
    [IME_PORUCIOCA] nvarchar(max)  NOT NULL,
    [DispecerJMBG] int  NOT NULL
);
GO

-- Creating table 'VoziloSet'
CREATE TABLE [dbo].[VoziloSet] (
    [BROJ_SASIJE] int IDENTITY(1,1) NOT NULL,
    [RG_TABLICE] int  NOT NULL,
    [MARKA] nvarchar(max)  NOT NULL,
    [MODEL] nvarchar(max)  NOT NULL,
    [TipVozilaID_KATEGORIJA] int  NOT NULL
);
GO

-- Creating table 'TipVozilaSet'
CREATE TABLE [dbo].[TipVozilaSet] (
    [ID_KATEGORIJA] int  NOT NULL,
    [KATEGORIJA] nvarchar(max)  NOT NULL,
    [POTREBNE_GOD] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'UpravljaSet'
CREATE TABLE [dbo].[UpravljaSet] (
    [VozacJMBG] int  NOT NULL,
    [TipVozilaID_KATEGORIJA] int  NOT NULL
);
GO

-- Creating table 'VoziSet'
CREATE TABLE [dbo].[VoziSet] (
    [VoziloBROJ_SASIJE] int  NOT NULL,
    [UpravljaVozacJMBG] int  NOT NULL,
    [UpravljaTipVozilaID_KATEGORIJA] int  NOT NULL
);
GO

-- Creating table 'RadnikSet_Direktor'
CREATE TABLE [dbo].[RadnikSet_Direktor] (
    [JMBG] int  NOT NULL
);
GO

-- Creating table 'RadnikSet_Promoter'
CREATE TABLE [dbo].[RadnikSet_Promoter] (
    [JMBG] int  NOT NULL
);
GO

-- Creating table 'RadnikSet_Dispecer'
CREATE TABLE [dbo].[RadnikSet_Dispecer] (
    [JMBG] int  NOT NULL
);
GO

-- Creating table 'RadnikSet_Vozac'
CREATE TABLE [dbo].[RadnikSet_Vozac] (
    [JMBG] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [JMBG] in table 'RadnikSet'
ALTER TABLE [dbo].[RadnikSet]
ADD CONSTRAINT [PK_RadnikSet]
    PRIMARY KEY CLUSTERED ([JMBG] ASC);
GO

-- Creating primary key on [ID_UGOVORA] in table 'UgovorSet'
ALTER TABLE [dbo].[UgovorSet]
ADD CONSTRAINT [PK_UgovorSet]
    PRIMARY KEY CLUSTERED ([ID_UGOVORA] ASC);
GO

-- Creating primary key on [DirektorJMBG], [UgovorID_UGOVORA] in table 'PotpisujeSet'
ALTER TABLE [dbo].[PotpisujeSet]
ADD CONSTRAINT [PK_PotpisujeSet]
    PRIMARY KEY CLUSTERED ([DirektorJMBG], [UgovorID_UGOVORA] ASC);
GO

-- Creating primary key on [ID_PROMOCIJE] in table 'PromocijaSet'
ALTER TABLE [dbo].[PromocijaSet]
ADD CONSTRAINT [PK_PromocijaSet]
    PRIMARY KEY CLUSTERED ([ID_PROMOCIJE] ASC);
GO

-- Creating primary key on [PromoterJMBG], [PromocijaID_PROMOCIJE] in table 'KreiraSet'
ALTER TABLE [dbo].[KreiraSet]
ADD CONSTRAINT [PK_KreiraSet]
    PRIMARY KEY CLUSTERED ([PromoterJMBG], [PromocijaID_PROMOCIJE] ASC);
GO

-- Creating primary key on [RB_PORUDZBINE] in table 'PorudzbinaSet'
ALTER TABLE [dbo].[PorudzbinaSet]
ADD CONSTRAINT [PK_PorudzbinaSet]
    PRIMARY KEY CLUSTERED ([RB_PORUDZBINE] ASC);
GO

-- Creating primary key on [BROJ_SASIJE] in table 'VoziloSet'
ALTER TABLE [dbo].[VoziloSet]
ADD CONSTRAINT [PK_VoziloSet]
    PRIMARY KEY CLUSTERED ([BROJ_SASIJE] ASC);
GO

-- Creating primary key on [ID_KATEGORIJA] in table 'TipVozilaSet'
ALTER TABLE [dbo].[TipVozilaSet]
ADD CONSTRAINT [PK_TipVozilaSet]
    PRIMARY KEY CLUSTERED ([ID_KATEGORIJA] ASC);
GO

-- Creating primary key on [VozacJMBG], [TipVozilaID_KATEGORIJA] in table 'UpravljaSet'
ALTER TABLE [dbo].[UpravljaSet]
ADD CONSTRAINT [PK_UpravljaSet]
    PRIMARY KEY CLUSTERED ([VozacJMBG], [TipVozilaID_KATEGORIJA] ASC);
GO

-- Creating primary key on [VoziloBROJ_SASIJE], [UpravljaVozacJMBG], [UpravljaTipVozilaID_KATEGORIJA] in table 'VoziSet'
ALTER TABLE [dbo].[VoziSet]
ADD CONSTRAINT [PK_VoziSet]
    PRIMARY KEY CLUSTERED ([VoziloBROJ_SASIJE], [UpravljaVozacJMBG], [UpravljaTipVozilaID_KATEGORIJA] ASC);
GO

-- Creating primary key on [JMBG] in table 'RadnikSet_Direktor'
ALTER TABLE [dbo].[RadnikSet_Direktor]
ADD CONSTRAINT [PK_RadnikSet_Direktor]
    PRIMARY KEY CLUSTERED ([JMBG] ASC);
GO

-- Creating primary key on [JMBG] in table 'RadnikSet_Promoter'
ALTER TABLE [dbo].[RadnikSet_Promoter]
ADD CONSTRAINT [PK_RadnikSet_Promoter]
    PRIMARY KEY CLUSTERED ([JMBG] ASC);
GO

-- Creating primary key on [JMBG] in table 'RadnikSet_Dispecer'
ALTER TABLE [dbo].[RadnikSet_Dispecer]
ADD CONSTRAINT [PK_RadnikSet_Dispecer]
    PRIMARY KEY CLUSTERED ([JMBG] ASC);
GO

-- Creating primary key on [JMBG] in table 'RadnikSet_Vozac'
ALTER TABLE [dbo].[RadnikSet_Vozac]
ADD CONSTRAINT [PK_RadnikSet_Vozac]
    PRIMARY KEY CLUSTERED ([JMBG] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [DirektorJMBG] in table 'PotpisujeSet'
ALTER TABLE [dbo].[PotpisujeSet]
ADD CONSTRAINT [FK_DirektorPotpisuje]
    FOREIGN KEY ([DirektorJMBG])
    REFERENCES [dbo].[RadnikSet_Direktor]
        ([JMBG])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [UgovorID_UGOVORA] in table 'PotpisujeSet'
ALTER TABLE [dbo].[PotpisujeSet]
ADD CONSTRAINT [FK_UgovorPotpisuje]
    FOREIGN KEY ([UgovorID_UGOVORA])
    REFERENCES [dbo].[UgovorSet]
        ([ID_UGOVORA])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UgovorPotpisuje'
CREATE INDEX [IX_FK_UgovorPotpisuje]
ON [dbo].[PotpisujeSet]
    ([UgovorID_UGOVORA]);
GO

-- Creating foreign key on [PromoterJMBG] in table 'KreiraSet'
ALTER TABLE [dbo].[KreiraSet]
ADD CONSTRAINT [FK_PromoterKreira]
    FOREIGN KEY ([PromoterJMBG])
    REFERENCES [dbo].[RadnikSet_Promoter]
        ([JMBG])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [PromocijaID_PROMOCIJE] in table 'KreiraSet'
ALTER TABLE [dbo].[KreiraSet]
ADD CONSTRAINT [FK_PromocijaKreira]
    FOREIGN KEY ([PromocijaID_PROMOCIJE])
    REFERENCES [dbo].[PromocijaSet]
        ([ID_PROMOCIJE])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PromocijaKreira'
CREATE INDEX [IX_FK_PromocijaKreira]
ON [dbo].[KreiraSet]
    ([PromocijaID_PROMOCIJE]);
GO

-- Creating foreign key on [DispecerJMBG] in table 'PorudzbinaSet'
ALTER TABLE [dbo].[PorudzbinaSet]
ADD CONSTRAINT [FK_DispecerPorudzbina]
    FOREIGN KEY ([DispecerJMBG])
    REFERENCES [dbo].[RadnikSet_Dispecer]
        ([JMBG])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DispecerPorudzbina'
CREATE INDEX [IX_FK_DispecerPorudzbina]
ON [dbo].[PorudzbinaSet]
    ([DispecerJMBG]);
GO

-- Creating foreign key on [VozacJMBG] in table 'UpravljaSet'
ALTER TABLE [dbo].[UpravljaSet]
ADD CONSTRAINT [FK_VozacUpravlja]
    FOREIGN KEY ([VozacJMBG])
    REFERENCES [dbo].[RadnikSet_Vozac]
        ([JMBG])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [VoziloBROJ_SASIJE] in table 'VoziSet'
ALTER TABLE [dbo].[VoziSet]
ADD CONSTRAINT [FK_VoziloVozi]
    FOREIGN KEY ([VoziloBROJ_SASIJE])
    REFERENCES [dbo].[VoziloSet]
        ([BROJ_SASIJE])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TipVozilaID_KATEGORIJA] in table 'UpravljaSet'
ALTER TABLE [dbo].[UpravljaSet]
ADD CONSTRAINT [FK_UpravljaTipVozila]
    FOREIGN KEY ([TipVozilaID_KATEGORIJA])
    REFERENCES [dbo].[TipVozilaSet]
        ([ID_KATEGORIJA])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UpravljaTipVozila'
CREATE INDEX [IX_FK_UpravljaTipVozila]
ON [dbo].[UpravljaSet]
    ([TipVozilaID_KATEGORIJA]);
GO

-- Creating foreign key on [TipVozilaID_KATEGORIJA] in table 'VoziloSet'
ALTER TABLE [dbo].[VoziloSet]
ADD CONSTRAINT [FK_TipVozilaVozilo]
    FOREIGN KEY ([TipVozilaID_KATEGORIJA])
    REFERENCES [dbo].[TipVozilaSet]
        ([ID_KATEGORIJA])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TipVozilaVozilo'
CREATE INDEX [IX_FK_TipVozilaVozilo]
ON [dbo].[VoziloSet]
    ([TipVozilaID_KATEGORIJA]);
GO

-- Creating foreign key on [UpravljaVozacJMBG], [UpravljaTipVozilaID_KATEGORIJA] in table 'VoziSet'
ALTER TABLE [dbo].[VoziSet]
ADD CONSTRAINT [FK_UpravljaVozi]
    FOREIGN KEY ([UpravljaVozacJMBG], [UpravljaTipVozilaID_KATEGORIJA])
    REFERENCES [dbo].[UpravljaSet]
        ([VozacJMBG], [TipVozilaID_KATEGORIJA])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UpravljaVozi'
CREATE INDEX [IX_FK_UpravljaVozi]
ON [dbo].[VoziSet]
    ([UpravljaVozacJMBG], [UpravljaTipVozilaID_KATEGORIJA]);
GO

-- Creating foreign key on [JMBG] in table 'RadnikSet_Direktor'
ALTER TABLE [dbo].[RadnikSet_Direktor]
ADD CONSTRAINT [FK_Direktor_inherits_Radnik]
    FOREIGN KEY ([JMBG])
    REFERENCES [dbo].[RadnikSet]
        ([JMBG])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [JMBG] in table 'RadnikSet_Promoter'
ALTER TABLE [dbo].[RadnikSet_Promoter]
ADD CONSTRAINT [FK_Promoter_inherits_Radnik]
    FOREIGN KEY ([JMBG])
    REFERENCES [dbo].[RadnikSet]
        ([JMBG])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [JMBG] in table 'RadnikSet_Dispecer'
ALTER TABLE [dbo].[RadnikSet_Dispecer]
ADD CONSTRAINT [FK_Dispecer_inherits_Radnik]
    FOREIGN KEY ([JMBG])
    REFERENCES [dbo].[RadnikSet]
        ([JMBG])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [JMBG] in table 'RadnikSet_Vozac'
ALTER TABLE [dbo].[RadnikSet_Vozac]
ADD CONSTRAINT [FK_Vozac_inherits_Radnik]
    FOREIGN KEY ([JMBG])
    REFERENCES [dbo].[RadnikSet]
        ([JMBG])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using BP2WPF.View;

namespace BP2WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Radnici_Button_Click(object sender, RoutedEventArgs e)
        {
            RadniciView radnici = new RadniciView();

            radnici.Show();
        }

        private void Ugovori_Button_Click(object sender, RoutedEventArgs e)
        {
            UgovoriView ugovori = new UgovoriView();

            ugovori.Show();
        }

        private void Potpisi_Ugovora_Button_Click(object sender, RoutedEventArgs e)
        {
            PotpisiUgovoraView potpisi = new PotpisiUgovoraView();

            potpisi.Show();
        }

        private void Promocije_Button_Click(object sender, RoutedEventArgs e)
        {
            PromocijeView promocije = new PromocijeView();

            promocije.Show();
        }

        private void Kreacije_Promocija_Button_Click(object sender, RoutedEventArgs e)
        {
            KreacijePromocijaView promocije = new KreacijePromocijaView();

            promocije.Show();
        }

        private void Porudzbine_Button_Click(object sender, RoutedEventArgs e)
        {
            PorudzbineView porudzbine = new PorudzbineView();

            porudzbine.Show();
        }

        private void Tipovi_Vozila_Button_Click(object sender, RoutedEventArgs e)
        {
            TipoviVozilaView tipoviVozila = new TipoviVozilaView();

            tipoviVozila.Show();
        }

        private void Vozila_Button_Click(object sender, RoutedEventArgs e)
        {
            VozilaView vozila = new VozilaView();

            vozila.Show();
        }

        private void Mogucnosti_Upravljanja_Button_Click(object sender, RoutedEventArgs e)
        {
            MogucnostiUpravljanjaView mogucnostiUpravljanja = new MogucnostiUpravljanjaView();

            mogucnostiUpravljanja.Show();
        }

        private void Upravljanja_Button_Click(object sender, RoutedEventArgs e)
        {
            UpravljanjaView upravljanja = new UpravljanjaView();

            upravljanja.Show();
        }
    }
}

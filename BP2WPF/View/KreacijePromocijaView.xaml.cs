﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for KreacijePromocijaView.xaml
    /// </summary>
    public partial class KreacijePromocijaView : Window
    {
        public KreacijePromocijaView()
        {
            InitializeComponent();

            KreacijePromocijaVM.Read();

            DataGrid_Kreacije.ItemsSource = KreacijePromocijaVM.kracijePromocijaList;

            JMBG_PROMOTERA_ComboBox.ItemsSource = RadniciVM.getPromoteri();

            ID_PROMOCIJE_ComboBox.ItemsSource = PromocijeVM.getPromocije();

            DataContext = this;
        }

        private void Dodaj_Kreaciju_Button_Click(object sender, RoutedEventArgs e)
        {
            int jmbg = -1;
            int id = -1;

            if (JMBG_PROMOTERA_ComboBox.SelectedItem == null || ID_PROMOCIJE_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }

            Int32.TryParse(JMBG_PROMOTERA_ComboBox.SelectedValue.ToString(), out jmbg);

            Int32.TryParse(ID_PROMOCIJE_ComboBox.SelectedValue.ToString(), out id);

            if (KreacijePromocijaVM.Create(jmbg, id) == -1)
            {
                MessageBox.Show("Doslo je do greske. Proverite da li kreacija promocije vec postoji.");
            }
            else
            {
                MessageBox.Show("Uspesno je dodata nova kreacija promocije!");

                this.Close();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            KreacijePromocijaVM.kracijePromocijaList = null;
        }

        private void Obrisi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (KreacijePromocijaVM.Delete(DataGrid_Kreacije.SelectedIndex) == -1)
            {
                MessageBox.Show("Doslo je do greske pri pokusaju brisanja kreacije promocije. Pokusajte ponovo.");
            }
            else
            {
                MessageBox.Show("Uspesno je obrisana kreacija promocije!");
            }
        }
    }
}

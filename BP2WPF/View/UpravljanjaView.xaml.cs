﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for UpravljanjaView.xaml
    /// </summary>
    public partial class UpravljanjaView : Window
    {
        public UpravljanjaView()
        {
            InitializeComponent();

            UpravljanjaVM.Read();

            DataGrid_Upravljanja.ItemsSource = UpravljanjaVM.upravljanjaList;

            JMBG_I_KATEGORIJA_ComboBox.ItemsSource = MogucnostiUpravljanjaVM.getMogucnostiUpravljanja();

            BR_SASIJE_I_KATEGORIJA_ComboBox.ItemsSource = VozilaVM.getVozila();

            DataContext = this;
        }

        private void Dodaj_Upravljanje_Button_Click(object sender, RoutedEventArgs e)
        {
            int jmbg = -1;
            int br = -1;
            int id1 = -1;
            int id2 = -1;

            if (JMBG_I_KATEGORIJA_ComboBox.SelectedItem == null || BR_SASIJE_I_KATEGORIJA_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            
            Int32.TryParse(JMBG_I_KATEGORIJA_ComboBox.SelectedValue.ToString().Split('-')[0], out jmbg);

            Int32.TryParse(BR_SASIJE_I_KATEGORIJA_ComboBox.SelectedValue.ToString().Split('-')[0], out br);

            Int32.TryParse(JMBG_I_KATEGORIJA_ComboBox.SelectedValue.ToString().Split('-')[1], out id1);

            Int32.TryParse(BR_SASIJE_I_KATEGORIJA_ComboBox.SelectedValue.ToString().Split('-')[1], out id2);

            if (id1 != id2)
            {
                MessageBox.Show("Vozac nema mogucnost upravljanja datim vozilom. Kategorije moraju biti iste. Pokusajte ponovo.");
                return;
            }

            if (UpravljanjaVM.Create(jmbg, br, id1) == -1)
            {
                MessageBox.Show("Doslo je do greske. Proverite da li upravljanje vec postoji.");
            }
            else
            {
                MessageBox.Show("Uspesno je dodato novo upravljanje!");

                this.Close();
            }
        }

        private void Obrisi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (UpravljanjaVM.Delete(DataGrid_Upravljanja.SelectedIndex) == -1)
            {
                MessageBox.Show("Doslo je do greske pri pokusaju brisanja upravljanja. Pokusajte ponovo.");
            }
            else
            {
                MessageBox.Show("Uspesno je obrisano upravljanje!");
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            UpravljanjaVM.upravljanjaList = null;
        }
    }
}

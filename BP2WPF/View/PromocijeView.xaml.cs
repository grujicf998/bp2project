﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;
using BP2WPF.View.ModifyView;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for PromocijeView.xaml
    /// </summary>
    public partial class PromocijeView : Window
    {
        public PromocijeView()
        {
            InitializeComponent();

            PromocijeVM.Read();

            DataGrid_Promocije.ItemsSource = PromocijeVM.promocijeList;

            KREATOR_ComboBox.ItemsSource = RadniciVM.getPromoteri();

            DataContext = this;
        }

        private void Dodaj_Promociju_Button_Click(object sender, RoutedEventArgs e)
        {
            int id = -1;
            int cena = -1;

            if (ID_TextBox.Text.Trim() == "" || CENA_TextBox.Text.Trim() == "" || VRSTA_ComboBox.Text.Trim() == "" || KREATOR_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(ID_TextBox.Text.Trim(), out id))
            {
                MessageBox.Show("Neophodno je da id promocije bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(CENA_TextBox.Text.Trim(), out cena))
            {
                MessageBox.Show("Neophodno je da cena promocije bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }

            Int32.TryParse(KREATOR_ComboBox.SelectedValue.ToString(), out int jmbg);

            if (PromocijeVM.Create(id, cena, VRSTA_ComboBox.SelectedValue.ToString().Split(' ')[1], jmbg) == -1)
            {
                MessageBox.Show("Doslo je do greske. Proverite da li promocija sa zadatim ID-om vec postoji.");
            }
            else
            {
                MessageBox.Show("Uspesno je dodata nova promocija!");

                this.Close();
            }
        }

        private void Obrisi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (PromocijeVM.Delete(DataGrid_Promocije.SelectedIndex) == -1)
            {
                MessageBox.Show("Doslo je do greske pri pokusaju brisanja promocije. Pokusajte ponovo.");
            }
            else
            {
                MessageBox.Show("Uspesno je obrisana promocija!");
            }
        }

        private void Izmeni_Button_Click(object sender, RoutedEventArgs e)
        {
            PromocijeModifyView window = new PromocijeModifyView(DataGrid_Promocije.SelectedIndex);

            window.Show();
        }
    }
}

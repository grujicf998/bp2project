﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View.ModifyView
{
    /// <summary>
    /// Interaction logic for TipoviVozilaModifyView.xaml
    /// </summary>
    public partial class TipoviVozilaModifyView : Window
    {
        public int Index { get; set; }

        public TipoviVozilaModifyView(int index)
        {
            InitializeComponent();
            Index = index;
            ID_TextBox.Text = TipoviVozilaVM.tipoviList[index].ID_KATEGORIJE.ToString();
            IME_TextBox.Text = TipoviVozilaVM.tipoviList[index].KATEGORIJA.ToString();
            GODINE_TextBox.Text = TipoviVozilaVM.tipoviList[index].POTREBNE_GOD.ToString();
        }

        private void Izmeni_Informacije_O_Tipu_Vozila_Button_Click(object sender, RoutedEventArgs e)
        {
            int godine = -1;

            if (IME_TextBox.Text.Trim() == "" || GODINE_TextBox.Text.Trim() == "")
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(GODINE_TextBox.Text.Trim(), out godine))
            {
                MessageBox.Show("Neophodno je da potrebne godine budu celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }

            if (TipoviVozilaVM.Update(Index, IME_TextBox.Text.ToString(), godine) == -1)
            {
                MessageBox.Show("Doslo je do greske prilikom pokusaja izmene. Pokusajte ponovo.");
                return;
            }
            else
            {
                MessageBox.Show("Uspesno je izvrsena izmena!");
                this.Close();
            }
        }
    }
}

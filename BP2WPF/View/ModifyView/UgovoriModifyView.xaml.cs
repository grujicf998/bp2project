﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View.ModifyView
{
    /// <summary>
    /// Interaction logic for UgovoriModifyView.xaml
    /// </summary>
    public partial class UgovoriModifyView : Window
    {
        public int Index { get; set; }

        public UgovoriModifyView(int index)
        {
            InitializeComponent();
            Index = index;
            ID_TextBox.Text = UgovoriVM.ugovoriList[index].ID_UGOVORA.ToString();
            IME_TextBox.Text = UgovoriVM.ugovoriList[index].IME_KOMPANIJE.ToString();
            DATUM_TextBox.Text = UgovoriVM.ugovoriList[index].DAT_ZAKLJUCENJA.ToString();
            VAZENJE_TextBox.Text = UgovoriVM.ugovoriList[index].PERIOD_VAZENJA.ToString();
        }

        private void Izmeni_Informacije_O_Ugovoru_Button_Click(object sender, RoutedEventArgs e)
        {
            int vazenje = -1;
            DateTime datum;

            if (IME_TextBox.Text.Trim() == "" || DATUM_TextBox.Text.Trim() == "" || VAZENJE_TextBox.Text.Trim() == "")
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(VAZENJE_TextBox.Text.Trim(), out vazenje))
            {
                MessageBox.Show("Neophodno je da period vazenja ugovora bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }
            else if (!DateTime.TryParse(DATUM_TextBox.Text, out datum))
            {
                MessageBox.Show("Neispravno unesen datum. Pokusajte ponovo.");
                return;
            }

            if (UgovoriVM.Update(Index, IME_TextBox.Text, datum, vazenje) == -1)
            {
                MessageBox.Show("Doslo je do greske prilikom pokusaja izmene. Pokusajte ponovo.");
                return;
            }
            else
            {
                MessageBox.Show("Uspesno je izvrsena izmena!");
                this.Close();
            }
        }
    }
}

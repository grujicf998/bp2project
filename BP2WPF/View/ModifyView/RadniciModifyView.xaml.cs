﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View.ModifyView
{
    /// <summary>
    /// Interaction logic for RadniciModifyView.xaml
    /// </summary>
    public partial class RadniciModifyView : Window
    {
        public int Index { get; set; }

        public RadniciModifyView(int index)
        {
            InitializeComponent();
            Index = index;
            JMBG_TextBox.Text = RadniciVM.radniciList[index].JMBG.ToString();
            ZANIMANJE_TextBox.Text = RadniciVM.radniciList[index].ZANIMANJE.ToString();
            IME_TextBox.Text = RadniciVM.radniciList[index].IME.ToString();
            PREZIME_TextBox.Text = RadniciVM.radniciList[index].PREZIME.ToString();
            PLATA_TextBox.Text = RadniciVM.radniciList[index].PLATA.ToString();
        }

        private void Izmeni_Informcije_O_Radniku_Button_Click(object sender, RoutedEventArgs e)
        {
            int plata = -1;

            if (IME_TextBox.Text.Trim() == "" || PREZIME_TextBox.Text.Trim() == "" || PLATA_TextBox.Text.Trim() == "")
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(PLATA_TextBox.Text.Trim(), out plata))
            {
                MessageBox.Show("Neophodno je da plata bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }

            if (RadniciVM.Update(Index, IME_TextBox.Text, PREZIME_TextBox.Text, plata) == -1)
            {
                MessageBox.Show("Doslo je do greske prilikom pokusaja izmene. Pokusajte ponovo.");
                return;
            }
            else
            {
                MessageBox.Show("Uspesno je izvrsena izmena!");
                this.Close();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View.ModifyView
{
    /// <summary>
    /// Interaction logic for PromocijeModifyView.xaml
    /// </summary>
    public partial class PromocijeModifyView : Window
    {
        public int Index { get; set; }

        public PromocijeModifyView(int index)
        {
            InitializeComponent();
            Index = index;
            ID_TextBox.Text = PromocijeVM.promocijeList[index].ID_PROMOCIJE.ToString();
            VRSTA_ComboBox.Text = PromocijeVM.promocijeList[index].VRSTA_PROMOCIJE.ToString();
            CENA_TextBox.Text = PromocijeVM.promocijeList[index].CENA_PROMOCIJE.ToString();
        }

        private void Izmeni_Informacije_O_Promociji_Button_Click(object sender, RoutedEventArgs e)
        {
            int cena = -1;

            if (CENA_TextBox.Text.Trim() == "" || VRSTA_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(CENA_TextBox.Text.Trim(), out cena))
            {
                MessageBox.Show("Neophodno je da cena promocije bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }


            if (PromocijeVM.Update(Index, cena, VRSTA_ComboBox.SelectedValue.ToString().Split(' ')[1]) == -1)
            {
                MessageBox.Show("Doslo je do greske prilikom pokusaja izmene. Pokusajte ponovo.");
                return;
            }
            else
            {
                MessageBox.Show("Uspesno je izvrsena izmena!");
                this.Close();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View.ModifyView
{
    /// <summary>
    /// Interaction logic for PorudzbineModifyView.xaml
    /// </summary>
    public partial class PorudzbineModifyView : Window
    {
        public int Index { get; set; }

        public PorudzbineModifyView(int index)
        {
            InitializeComponent();
            Index = index;
            RB_TextBox.Text = PorudzbineVM.porudzbineList[index].RB_PORUDZBINE.ToString();
            CENA_TextBox.Text = PorudzbineVM.porudzbineList[index].CENA_PORUDZBINE.ToString();
            PORUCILAC_TextBox.Text = PorudzbineVM.porudzbineList[index].IME_PORUCIOCA.ToString();
            PREDMET_TextBox.Text = PorudzbineVM.porudzbineList[index].PREDMET_PORUDZBINE.ToString();
            DISPECER_ComboBox.ItemsSource = RadniciVM.getDispeceri();
            DISPECER_ComboBox.Text = PorudzbineVM.porudzbineList[index].DispecerJMBG.ToString();
        }

        private void Izmeni_Informacije_O_Porudzbini_Button_Click(object sender, RoutedEventArgs e)
        {
            int cena = -1;

            if (PORUCILAC_TextBox.Text.Trim() == "" || PREDMET_TextBox.Text.Trim() == "" || CENA_TextBox.Text.Trim() == "" || DISPECER_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(CENA_TextBox.Text.Trim(), out cena))
            {
                MessageBox.Show("Neophodno je da cena porudzbine bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }

            Int32.TryParse(DISPECER_ComboBox.SelectedValue.ToString(), out int jmbg);

            if (PorudzbineVM.Update(Index, cena, PORUCILAC_TextBox.Text, PREDMET_TextBox.Text, jmbg) == -1)
            {
                MessageBox.Show("Doslo je do greske prilikom pokusaja izmene. Pokusajte ponovo.");
                return;
            }
            else
            {
                MessageBox.Show("Uspesno je izvrsena izmena!");
                this.Close();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View.ModifyView
{
    /// <summary>
    /// Interaction logic for VozilaModifyView.xaml
    /// </summary>
    public partial class VozilaModifyView : Window
    {
        public int Index { get; set; }

        public VozilaModifyView(int index)
        {
            InitializeComponent();
            Index = index;
            R_TABLICE_TextBox.Text = VozilaVM.vozilaList[index].RG_TABLICE.ToString();
            MARKA_TextBox.Text = VozilaVM.vozilaList[index].MARKA.ToString();
            MODEL_TextBox.Text = VozilaVM.vozilaList[index].MODEL.ToString();
            BROJ_SASIJE_TextBox.Text = VozilaVM.vozilaList[index].BROJ_SASIJE.ToString();
            KATEGORIJA_ComboBox.ItemsSource = TipoviVozilaVM.getKategorije();
            KATEGORIJA_ComboBox.Text = VozilaVM.vozilaList[index].TipVozilaID_KATEGORIJE.ToString();
        }

        private void Izmeni_Informacije_O_Vozilu_Button_Click(object sender, RoutedEventArgs e)
        {
            if (MARKA_TextBox.Text.Trim() == "" || MODEL_TextBox.Text.Trim() == "" || R_TABLICE_TextBox.Text.Trim() == "" || KATEGORIJA_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }

            Int32.TryParse(KATEGORIJA_ComboBox.SelectedValue.ToString(), out int kategorija);

            if (VozilaVM.Update(Index, MARKA_TextBox.Text, MODEL_TextBox.Text, R_TABLICE_TextBox.Text, kategorija) == -1)
            {
                MessageBox.Show("Doslo je do greske prilikom pokusaja izmene. Pokusajte ponovo.");
                return;
            }
            else
            {
                MessageBox.Show("Uspesno je izvrsena izmena!");
                this.Close();
            }
        }
    }
}

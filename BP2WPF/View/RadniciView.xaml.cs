﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;
using BP2WPF.View.ModifyView;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for RadniciView.xaml
    /// </summary>
    public partial class RadniciView : Window
    {
        public RadniciView()
        {
            InitializeComponent();

            RadniciVM.Read();

            DataGrid_Radnici.ItemsSource = RadniciVM.radniciList;

            DataContext = this;
        }

        private void Dodaj_Radnika_Button_Click(object sender, RoutedEventArgs e)
        {
            int jmbg = -1;
            int plata = -1;
            string zanimanje = "";

            if (JMBG_TextBox.Text.Trim() == "" || IME_TextBox.Text.Trim() == "" || PREZIME_TextBox.Text.Trim() == "" || PLATA_TextBox.Text.Trim() == "")
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(JMBG_TextBox.Text.Trim(), out jmbg))
            {
                MessageBox.Show("Neophodno je da jmbg bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(PLATA_TextBox.Text.Trim(), out plata))
            {
                MessageBox.Show("Neophodno je da plata bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }

            switch (ZANIMANJE_ComboBox.SelectedIndex)
            {
                case 0:
                    zanimanje = "Direktor";
                    break;
                case 1:
                    zanimanje = "Vozac";
                    break;
                case 2:
                    zanimanje = "Promoter";
                    break;
                case 3:
                    zanimanje = "Dispecer";
                    break;
            }

            if (zanimanje == "Vozac")
            {
                RadniciVozacIzborView izbor = new RadniciVozacIzborView();

                izbor.JMBG = jmbg;
                izbor.IME = IME_TextBox.Text;
                izbor.PREZIME = PREZIME_TextBox.Text;
                izbor.PLATA = plata;

                if(izbor.ShowDialog() == true)
                {
                    this.Close();
                }
            }
            else
            {
                if (RadniciVM.Create(jmbg, IME_TextBox.Text, PREZIME_TextBox.Text, plata, zanimanje, -1) == -1)
                {
                    MessageBox.Show("Doslo je do greske. Proverite da li radnik sa zadatim JMBG-om vec postoji.");
                }
                else
                {
                    MessageBox.Show("Uspesno je dodat novi radnik!");

                    this.Close();
                }
            }
        }

        private void Obrisi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (RadniciVM.Delete(DataGrid_Radnici.SelectedIndex) == -1)
            {
                MessageBox.Show("Doslo je do greske pri pokusaju brisanja radnika. Pokusajte ponovo.");
            }
            else
            {
                MessageBox.Show("Uspesno je obrisan radnik!");
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            RadniciVM.radniciList = null;
        }

        private void Izmeni_Button_Click(object sender, RoutedEventArgs e)
        {
            RadniciModifyView window = new RadniciModifyView(DataGrid_Radnici.SelectedIndex);

            window.Show();
        }
    }
}

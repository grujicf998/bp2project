﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for PotpisiUgovoraView.xaml
    /// </summary>
    public partial class PotpisiUgovoraView : Window
    {
        public PotpisiUgovoraView()
        {
            InitializeComponent();

            PotpisiUgovoraVM.Read();

            DataGrid_Potpisi.ItemsSource = PotpisiUgovoraVM.potpisiUgovoraList;

            JMBG_DIREKTORA_ComboBox.ItemsSource = RadniciVM.getDirektori();

            ID_UGOVORA_ComboBox.ItemsSource = UgovoriVM.getUgovori();

            DataContext = this;
        }

        private void Dodaj_Potpis_Ugovora_Button_Click(object sender, RoutedEventArgs e)
        {
            int jmbg = -1;
            int id = -1;

            if (JMBG_DIREKTORA_ComboBox.SelectedItem == null || ID_UGOVORA_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }

            Int32.TryParse(JMBG_DIREKTORA_ComboBox.SelectedValue.ToString(), out jmbg);

            Int32.TryParse(ID_UGOVORA_ComboBox.SelectedValue.ToString(), out id);

            if (PotpisiUgovoraVM.Create(jmbg, id) == -1)
            {
                MessageBox.Show("Doslo je do greske. Proverite da li potpis vec postoji.");
            }
            else
            {
                MessageBox.Show("Uspesno je dodat novi potpis ugovora!");

                this.Close();
            }
        }

        private void Obrisi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (PotpisiUgovoraVM.Delete(DataGrid_Potpisi.SelectedIndex) == -1)
            {
                MessageBox.Show("Doslo je do greske pri pokusaju brisanja potpisa. Pokusajte ponovo.");
            }
            else
            {
                MessageBox.Show("Uspesno je obrisan potpis!");
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            PotpisiUgovoraVM.potpisiUgovoraList = null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;
using BP2WPF.View.ModifyView;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for TipoviVozilaView.xaml
    /// </summary>
    public partial class TipoviVozilaView : Window
    {
        public TipoviVozilaView()
        {
            InitializeComponent();

            TipoviVozilaVM.Read();

            DataGrid_TipoviVozila.ItemsSource = TipoviVozilaVM.tipoviList;

            DataContext = this;
        }

        private void Dodaj_Tip_Vozila_Button_Click(object sender, RoutedEventArgs e)
        {
            int id = -1;
            int godine = -1;

            if (ID_TextBox.Text.Trim() == "" || IME_TextBox.Text.Trim() == "" || GODINE_TextBox.Text.Trim() == "")
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(ID_TextBox.Text.Trim(), out id))
            {
                MessageBox.Show("Neophodno je da id ugovora bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(GODINE_TextBox.Text.Trim(), out godine))
            {
                MessageBox.Show("Neophodno je da potrebne godine budu celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }

            if (TipoviVozilaVM.Create(id, IME_TextBox.Text, godine) == -1)
            {
                MessageBox.Show("Doslo je do greske. Proverite da li tip vozila sa zadatim ID-om vec postoji.");
            }
            else
            {
                MessageBox.Show("Uspesno je dodat novi tip vozila!");

                this.Close();
            }
        }

        private void Obrisi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (TipoviVozilaVM.Delete(DataGrid_TipoviVozila.SelectedIndex) == -1)
            {
                MessageBox.Show("Doslo je do greske pri pokusaju brisanja tipa vozila. Pokusajte ponovo.");
            }
            else
            {
                MessageBox.Show("Uspesno je obrisan tip vozila!");
            }
        }

        private void Izmeni_Button_Click(object sender, RoutedEventArgs e)
        {
            TipoviVozilaModifyView window = new TipoviVozilaModifyView(DataGrid_TipoviVozila.SelectedIndex);

            window.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;
using BP2WPF.View.ModifyView;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for UgovoriView.xaml
    /// </summary>
    public partial class UgovoriView : Window
    {
        public UgovoriView()
        {
            InitializeComponent();

            UgovoriVM.Read();

            DataGrid_Ugovori.ItemsSource = UgovoriVM.ugovoriList;

            POTPISNIK_ComboBox.ItemsSource = RadniciVM.getDirektori();

            DataContext = this;
        }

        private void Dodaj_Ugovor_Button_Click(object sender, RoutedEventArgs e)
        {
            int id = -1;
            int vazenje = -1;
            DateTime datum;

            if (ID_TextBox.Text.Trim() == "" || IME_TextBox.Text.Trim() == "" || DATUM_TextBox.Text.Trim() == "" || VAZENJE_TextBox.Text.Trim() == "" || POTPISNIK_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(ID_TextBox.Text.Trim(), out id))
            {
                MessageBox.Show("Neophodno je da id ugovora bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(VAZENJE_TextBox.Text.Trim(), out vazenje))
            {
                MessageBox.Show("Neophodno je da period vazenja ugovora bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }
            else if (!DateTime.TryParse(DATUM_TextBox.Text, out datum))
            {
                MessageBox.Show("Neispravno unesen datum. Pokusajte ponovo.");
                return;
            }

            Int32.TryParse(POTPISNIK_ComboBox.SelectedValue.ToString(), out int jmbg);

            if (UgovoriVM.Create(id, IME_TextBox.Text, vazenje, datum, jmbg) == -1)
            {
                MessageBox.Show("Doslo je do greske. Proverite da li ugovor sa zadatim ID-om vec postoji.");
            }
            else
            {
                MessageBox.Show("Uspesno je dodat novi ugovor!");

                this.Close();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            UgovoriVM.ugovoriList = null;
        }

        private void Obrisi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (UgovoriVM.Delete(DataGrid_Ugovori.SelectedIndex) == -1)
            {
                MessageBox.Show("Doslo je do greske pri pokusaju brisanja ugovora. Pokusajte ponovo.");
            }
            else
            {
                MessageBox.Show("Uspesno je obrisan ugovor!");
            }
        }

        private void Izmeni_Button_Click(object sender, RoutedEventArgs e)
        {
            UgovoriModifyView window = new UgovoriModifyView(DataGrid_Ugovori.SelectedIndex);

            window.Show();
        }
    }
}

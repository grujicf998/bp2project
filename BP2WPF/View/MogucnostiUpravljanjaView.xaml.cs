﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for MogucnostiUpravljanjaView.xaml
    /// </summary>
    public partial class MogucnostiUpravljanjaView : Window
    {
        public MogucnostiUpravljanjaView()
        {
            InitializeComponent();

            MogucnostiUpravljanjaVM.Read();

            DataGrid_MogucnostiUpravljanja.ItemsSource = MogucnostiUpravljanjaVM.mogucnostiUpravljanjaList;

            JMBG_VOZACA_ComboBox.ItemsSource = RadniciVM.getVozaci();

            ID_KATEGORIJE_ComboBox.ItemsSource = TipoviVozilaVM.getKategorije();

            DataContext = this;
        }

        private void Dodaj_Mogucnost_Upravljanja_Button_Click(object sender, RoutedEventArgs e)
        {
            int jmbg = -1;
            int id = -1;

            if (JMBG_VOZACA_ComboBox.SelectedItem == null || ID_KATEGORIJE_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }

            Int32.TryParse(JMBG_VOZACA_ComboBox.SelectedValue.ToString(), out jmbg);

            Int32.TryParse(ID_KATEGORIJE_ComboBox.SelectedValue.ToString(), out id);

            if (MogucnostiUpravljanjaVM.Create(jmbg, id) == -1)
            {
                MessageBox.Show("Doslo je do greske. Proverite da li mogucnost upravljanja vec postoji.");
            }
            else
            {
                MessageBox.Show("Uspesno je dodata nova mogucnost upravljanja!");

                this.Close();
            }
        }

        private void Obrisi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (MogucnostiUpravljanjaVM.Delete(DataGrid_MogucnostiUpravljanja.SelectedIndex) == -1)
            {
                MessageBox.Show("Doslo je do greske pri pokusaju brisanja mogucnosti upravljanja. Pokusajte ponovo.");
            }
            else
            {
                MessageBox.Show("Uspesno je obrisana mogucnost upravljanja!");
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MogucnostiUpravljanjaVM.mogucnostiUpravljanjaList = null;
        }
    }
}

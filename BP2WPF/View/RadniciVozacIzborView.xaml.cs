﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for RadniciVozacIzborView.xaml
    /// </summary>
    public partial class RadniciVozacIzborView : Window
    {
        public int JMBG { get; set; }
        public string IME { get; set; }
        public string PREZIME { get; set; }
        public int PLATA { get; set; }

        public RadniciVozacIzborView()
        {
            InitializeComponent();

            TipoviVozilaVM.Read();

            KATEGORIJA_ComboBox.ItemsSource = TipoviVozilaVM.getKategorije();
        }

        private void Nastavi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (KATEGORIJA_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Neophodno je da se odabere kategorija vozila kojom vozac zna da upravlja.");
                return;
            }

            Int32.TryParse(KATEGORIJA_ComboBox.SelectedValue.ToString(), out int id);

            if (RadniciVM.Create(JMBG, IME, PREZIME, PLATA, "Vozac", id) == -1)
            {
                MessageBox.Show("Doslo je do greske. Proverite da li radnik sa zadatim JMBG-om vec postoji.");
            }
            else
            {
                MessageBox.Show("Uspesno je dodat novi radnik!");
                DialogResult = true;
                this.Close();
            }
        }
    }
}

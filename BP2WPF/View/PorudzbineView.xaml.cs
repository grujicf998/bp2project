﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;
using BP2WPF.View.ModifyView;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for PorudzbineView.xaml
    /// </summary>
    public partial class PorudzbineView : Window
    {
        public PorudzbineView()
        {
            InitializeComponent();

            PorudzbineVM.Read();

            DataGrid_Porudzbine.ItemsSource = PorudzbineVM.porudzbineList;

            DISPECER_ComboBox.ItemsSource = RadniciVM.getDispeceri();

            DataContext = this;
        }

        private void Dodaj_Porudzbinu_Button_Click(object sender, RoutedEventArgs e)
        {
            int rb = -1;
            int cena = -1;

            if (RB_TextBox.Text.Trim() == "" || PORUCILAC_TextBox.Text.Trim() == "" || PREDMET_TextBox.Text.Trim() == "" || CENA_TextBox.Text.Trim() == "" || DISPECER_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(RB_TextBox.Text.Trim(), out rb))
            {
                MessageBox.Show("Neophodno je da redni broj porudzbine bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(CENA_TextBox.Text.Trim(), out cena))
            {
                MessageBox.Show("Neophodno je da cena porudzbine bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }

            Int32.TryParse(DISPECER_ComboBox.SelectedValue.ToString(), out int jmbg);

            if (PorudzbineVM.Create(rb, cena, PORUCILAC_TextBox.Text, PREDMET_TextBox.Text, jmbg) == -1)
            {
                MessageBox.Show("Doslo je do greske. Proverite da li porudzbina sa zadatim RB-om vec postoji.");
            }
            else
            {
                MessageBox.Show("Uspesno je dodata nova porudzbina!");

                this.Close();
            }
        }

        private void Obrisi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (PorudzbineVM.Delete(DataGrid_Porudzbine.SelectedIndex) == -1)
            {
                MessageBox.Show("Doslo je do greske pri pokusaju brisanja porudzbine. Pokusajte ponovo.");
            }
            else
            {
                MessageBox.Show("Uspesno je obrisana porudzbina!");
            }
        }

        private void Izmeni_Button_Click(object sender, RoutedEventArgs e)
        {
            PorudzbineModifyView window = new PorudzbineModifyView(DataGrid_Porudzbine.SelectedIndex);

            window.Show();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            PorudzbineVM.porudzbineList = null;
        }
    }
}

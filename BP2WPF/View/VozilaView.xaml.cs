﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BP2CL.ViewModel;
using BP2WPF.View.ModifyView;

namespace BP2WPF.View
{
    /// <summary>
    /// Interaction logic for VozilaView.xaml
    /// </summary>
    public partial class VozilaView : Window
    {
        public VozilaView()
        {
            InitializeComponent();

            VozilaVM.Read();

            DataGrid_Vozila.ItemsSource = VozilaVM.vozilaList;

            KATEGORIJA_ComboBox.ItemsSource = TipoviVozilaVM.getKategorije();

            DataContext = this;
        }

        private void Dodaj_Vozilo_Button_Click(object sender, RoutedEventArgs e)
        {
            int br_sasije = -1;

            if (BROJ_SASIJE_TextBox.Text.Trim() == "" || MARKA_TextBox.Text.Trim() == "" || MODEL_TextBox.Text.Trim() == "" || R_TABLICE_TextBox.Text.Trim() == "" || KATEGORIJA_ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Nisu popunjena sva potrebna polja. Pokusajte ponovo.");
                return;
            }
            else if (!Int32.TryParse(BROJ_SASIJE_TextBox.Text.Trim(), out br_sasije))
            {
                MessageBox.Show("Neophodno je da broj sasije bude celobrojna vrednost veca od 0. Pokusajte ponovo.");
                return;
            }

            Int32.TryParse(KATEGORIJA_ComboBox.SelectedValue.ToString(), out int kategorija);

            if (VozilaVM.Create(br_sasije, MARKA_TextBox.Text, MODEL_TextBox.Text, R_TABLICE_TextBox.Text, kategorija) == -1)
            {
                MessageBox.Show("Doslo je do greske. Proverite da li vozilo sa zadatim brojem sasije vec postoji.");
            }
            else
            {
                MessageBox.Show("Uspesno je dodato novo vozilo!");

                this.Close();
            }
        }

        private void Obrisi_Button_Click(object sender, RoutedEventArgs e)
        {
            if (VozilaVM.Delete(DataGrid_Vozila.SelectedIndex) == -1)
            {
                MessageBox.Show("Doslo je do greske pri pokusaju brisanja vozila. Pokusajte ponovo.");
            }
            else
            {
                MessageBox.Show("Uspesno je obrisano vozilo!");
            }
        }

        private void Izmeni_Button_Click(object sender, RoutedEventArgs e)
        {
            VozilaModifyView window = new VozilaModifyView(DataGrid_Vozila.SelectedIndex);

            window.Show();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            VozilaVM.vozilaList = null;
        }
    }
}
